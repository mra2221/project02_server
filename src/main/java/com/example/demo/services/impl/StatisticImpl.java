package com.example.demo.services.impl;

import com.example.demo.dto.StatisticData;
import com.example.demo.services.Statistic;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class StatisticImpl implements Statistic {
    @Override
    public StatisticData getStatistic(String dataFile) throws IOException {

        if(dataFile.equals("bmi.csv")) {
            StatisticData data = new StatisticData();
            File file = new File(dataFile);
            if(file.length() > 0) {
                int auxiliary = 0;
                double temp;
                String tempString, next;
                BufferedReader read = new BufferedReader(new FileReader(dataFile));
                while ((next = read.readLine()) != null) {
                    for (int i = 0; i < next.length(); i++) {
                        if (next.charAt(i) == ';') {
                            auxiliary++;
                        }
                        if (auxiliary == 2) {
                            auxiliary = 0;
                            tempString = "";
                            for (int k = i + 1; k < i + 6; k++) {
                                tempString = tempString + next.charAt(k);
                            }
                            temp = Double.parseDouble(tempString);

                            if (temp >= 18.5 && temp <= 24.99) {
                                data.setNormal(Integer.parseInt(data.getNormal()) + 1);
                            } else if (temp >= 25 && temp <= 29.99) {
                                data.setOverweigt(Integer.parseInt(data.getOverweigt()) + 1);
                            } else if (temp >= 17 && temp <= 18.49) {
                                data.setUnderweight(Integer.parseInt(data.getUnderweight()) + 1);
                            } else if (temp >= 30 && temp <= 34.99) {
                                data.setObesityI(Integer.parseInt(data.getObesityI()) + 1);
                            } else if (temp >= 16 && temp <= 16.99) {
                                data.setEmaciation(Integer.parseInt(data.getEmaciation()) + 1);
                            } else if (temp >= 35 && temp <= 39.99) {
                                data.setObesityII(Integer.parseInt(data.getObesityII()) + 1);
                            } else if (temp >= 40) {
                                data.setObesityIII(Integer.parseInt(data.getObesityIII()) + 1);
                            } else if (temp < 16) {
                                data.setStarvation(Integer.parseInt(data.getStarvation()) + 1);
                            }
                        }
                    }
                }
            }else return null;
                return data;
        }
        else return null;
    }
}
