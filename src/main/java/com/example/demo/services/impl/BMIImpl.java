package com.example.demo.services.impl;

import com.example.demo.services.BMI;
import org.springframework.stereotype.Service;

@Service
public class BMIImpl implements BMI {
    @Override
    public  String getBMI(String weight, String height) {
        double BMI;
        try {
            BMI = Double.parseDouble(weight) / Math.pow(Double.parseDouble(height), 2);
        }catch (NumberFormatException e){
            return "ERROR";
        }


            return String.valueOf(BMI);

    }



}
