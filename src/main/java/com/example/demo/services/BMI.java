package com.example.demo.services;

public interface BMI {
    String getBMI(String weight, String height);
}