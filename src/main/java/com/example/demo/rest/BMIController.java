package com.example.demo.rest;

import com.example.demo.services.impl.BMIImpl;
import com.example.demo.services.impl.StatisticImpl;
import com.example.demo.dto.StatisticData;
import com.example.demo.dto.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.*;


@Controller
@RequestMapping("")
public class BMIController {
    @Autowired
    private BMIImpl bmiImp;
    @Autowired
    private StatisticImpl statisticImpl;
    
    @RequestMapping(value = "/bmi", method = RequestMethod.POST)
    public ResponseEntity<String> BMI(@RequestBody UserData userData) throws IOException {
        final String BMI = bmiImp.getBMI(userData.getWeight(),userData.getHeight());
        PrintWriter write = new PrintWriter(new BufferedWriter(new FileWriter("bmi.csv", true)));
        if(BMI != "ERROR") {
            write.println(userData.getFirstName() + ";" + userData.getLastName() + ";" + BMI);
            write.close();
        }
        return BMI == "ERROR" ?
                new ResponseEntity<>(HttpStatus.BAD_REQUEST) :
                new ResponseEntity<>(BMI, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/bmi/chart", method = RequestMethod.GET)
    public ResponseEntity<StatisticData> returnStatistic() throws IOException {
    final StatisticData statistic = statisticImpl.getStatistic("bmi.csv");
        return statistic == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(statistic, HttpStatus.OK);
    }

}