package com.example.demo.dto;

public class UserData {
    private String firstName;
    private String lastName;
    private String height;
    private String weight;
    public UserData() {
    }

    public UserData(final String firstName,
                    final String lastName,
                    final String height,
                    final String weight) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
        this.weight = weight;
    }
    public String getFirstName(){ return  firstName; }

    public String getLastName() { return lastName; }

    public String getHeight() { return height; }

    public String getWeight() { return weight; }
    @Override
    public String toString(){
        return "UserData{" +
                "firstName='"+firstName+'\''+
                ",lastName ='"+lastName+'\''+
                ",height ='"+height+'\''+
                ",weight ='"+weight+'\''+
                "}";
    }
}