package com.example.demo.dto;

public class StatisticData {
    private String starvation;
    private String emaciation;
    private String underweight;
    private String normal;
    private String overweigt;
    private String obesityI;
    private String obesityII;
    private String obesityIII;

    public StatisticData(){
        this.starvation="0";
        this.emaciation="0";
        this.underweight="0";
        this.normal="0";
        this.overweigt="0";
        this.obesityI="0";
        this.obesityII="0";
        this.obesityIII="0";
    }
    public StatisticData(String starvation,
                         String emaciation,
                         String underweight,
                         String normal,
                         String overweight,
                         String obesityI,
                         String obesityII,
                         String obesityIII){
        this.starvation=starvation;
        this.emaciation=emaciation;
        this.underweight=underweight;
        this.normal=normal;
        this.overweigt=overweight;
        this.obesityI=obesityI;
        this.obesityII=obesityII;
        this.obesityIII=obesityIII;
    }
    public String getStarvation(){
        return starvation;
    }

    public String getEmaciation() {
        return emaciation;
    }

    public String getNormal() {
        return normal;
    }

    public String getUnderweight() {
        return underweight;
    }

    public String getOverweigt() {
        return overweigt;
    }

    public String getObesityI() {
        return obesityI;
    }

    public String getObesityII() {
        return obesityII;
    }

    public String getObesityIII() {
        return obesityIII;
    }
    public void setStarvation(int starvation){
        this.starvation = String.valueOf(starvation);
    }
    public void setEmaciation(int emaciation){
        this.emaciation = String.valueOf(emaciation);
    }
    public void setUnderweight(int underweight){
        this.underweight = String.valueOf(underweight);
    }
    public  void setNormal(int normal){
        this.normal = String.valueOf(normal);
    }
    public void setOverweigt(int overweigt){
        this.overweigt = String.valueOf(overweigt);
    }
    public void setObesityI(int obesityI){
        this.obesityI = String.valueOf(obesityI);
    }
    public void setObesityII(int obesityII){
        this.obesityII = String.valueOf(obesityII);
    }
    public void setObesityIII(int obesityIII){
        this.obesityIII = String.valueOf(obesityIII);
    }
}